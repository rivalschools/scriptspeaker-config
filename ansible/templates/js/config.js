angular.module('scriptspeaker.constant', [])
.constant('EnvConfig', {"root_url": "{{ http_protocol }}://{{ server_name }}", "beta": false, "acapela_down": false});