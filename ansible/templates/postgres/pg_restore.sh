#  only arg is path/to/dump

#  run as sudo su - postgres
pg_restore --no-owner role=rivals -d scriptspeaker $1
#  reset serial auto-increment id to max id of imported data
SELECT pg_catalog.setval(pg_get_serial_sequence('user', 'id'), MAX(id)) FROM "user";