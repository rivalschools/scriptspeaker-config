#! /bin/bash

# directory to save backups in, must be rwx by postgres user
BASE_DIR="/home/$(whoami)/backups/postgres"
YMD=$(date "+%Y-%m-%d")
DIR="$BASE_DIR/$YMD"
mkdir -p $DIR
cd $DIR

# make database backup
# pg_dump $1 > "$DIR/pg.dump"

pg_dump -Fc --dbname=postgresql://"{{ db_user }}":"{{ db_password }}"@localhost/"{{ app }}" > "$DIR/pg.dump"

# delete backup files older than 7 days
OLD=$(find $BASE_DIR -type d -mtime +7)
if [ -n "$OLD" ] ; then
        echo deleting old backup files: $OLD
        echo $OLD | xargs rm -rfv
fi