Ansible configuration for Script Speaker.

This repo includes all sensitive information, which can be found in vars/vars.yml.

You will need ansible installed and a valid hosts files which is usually located at /etc/ansible/hosts. A hosts template can be found at ./hosts.template.

Playbooks can be deployed using the following shell command:

    ansible-playbook ansible/<playbook-name>-playbook.yml --extra-vars "target=<host>"